#!/bin/bash

dir="/home/ryb1n/music"
IP=`ifconfig | grep inet | grep -v inet6 | grep -v 127.0.0.1 | cut -d: -f2 | awk '{printf $1"\n"}'`
UP=`uptime | cut -d, -f1 `
DIRCL="/home/ryb1n/Общедоступные"
i=0
while [ "$i" -lt "3" ]
do
  df -h|grep $dir >/dev/null
  if [ $? == 0 ];then
    echo "yes"
    echo "Andrey Ryb1n IP:$IP" >> $DIRCL/log
    echo "$UP" >> $DIRCL/log
    echo "" >> $DIRCL/log
    exit 0;
  else 
    echo "no"
    sudo mount -t nfs $IP:$dir $DIRCL
    i=$(($i + 1)) 
    sleep 1
  fi
done
