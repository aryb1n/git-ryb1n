#!/bin/bash
#Срабатывает заданная мелодия будильника в hour и minute-
#-В формате scr3.sh hh mm signal.mp3
h=`date +'%H'`
m=`date +'%M'`

while [[ !(($h == "$1") && ($m == "$2")) ]];
do
  h=`date +'%H'`
  m=`date +'%M'`
  sleep 1

done

echo "Пора вставать!"
mplayer $3

