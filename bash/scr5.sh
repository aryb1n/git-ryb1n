#!/bin/bash
#Ищет логи для процесса в промежутке заданных часов. Ввод в формате /scr5.sh NameProc hour1 hour2
det=0
while [ "$det" -eq "0" ]
do
echo "Скрипт поиска логов"
echo "Введи имя процесса"
read p;
find /var/log -maxdepth 1 -name "$p" | grep $p >/dev/null #Поиск по имени
if [[ $? != 0 ]]; then
	echo "Лог файл с именем $p не найден";
else det+=1;
	echo "Лог файл с именем $p найден";
fi
cat /var/log/syslog* | grep $p >/dev/null #Поиск в syslog
if [[ $? != 0 ]]; then
	echo "Логи процесса $p в syslog не найдены";
else det+=1;
	echo "Логи процесса $p в syslog найдены";
fi
done

while :
do
echo "Нужно ввести промежуток времени"
echo "Введи начальный час(Например 3 или 13)"
read sthour;
echo "Введи конечный час(Например 9 или 23)"
read endhour;
if [ "$sthour" -ge "0" ]&&[ "$sthour" -le "23" ]&&[ "$endhour" -ge "0" ]&&[ "$endhour" -le "23" ]&&[ "$sthour" -le "$endhour" ]; then
	echo "Вывести данные для указанного времени?[y/n]"
	read YN;
	if [ "$YN" = "y" ]||[ "$YN" = "Y" ]; then
		for hour in `seq $sthour $endhour`;
		do 
			if [ $hour -lt 10 ]; then
				cat /var/log/$p 2>/dev/null | grep "\<0$hour:[0-9][0-9]:[0-9][0-9]\>" 
				cat /var/log/syslog* 2>/dev/null | grep $p | grep "\<0$hour:[0-9][0-9]:[0-9][0-9]\>" 
			else
				cat /var/log/$p 2>/dev/null | grep "\<$hour:[0-9][0-9]:[0-9][0-9]\>" 
				cat /var/log/syslog* 2>/dev/null | grep $p | grep "\<$hour:[0-9][0-9]:[0-9][0-9]\>" 
			fi	
		done
		break;
	else echo "Отмена"
		break;
	fi
else echo  "Время введено не правильно"
fi
done
