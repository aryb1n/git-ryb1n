#!/bin/bash
#Проверяет наличие процесса, если нет, то запускает.
#Управляюется скриптом prcron.sh

ps -C $1 >/dev/null
if [[ $? != 0 ]]
then
	zenity --info --title "Контроль процесса $1" --text "Процесс $1 не найдет.  Запуск $1..." &
	$1 >/dev/null
	exit 0;
else
	zenity --info --title "Контроль процесса $1" --text "Процесс $1 работает." &		
	exit 0;
fi

