//Читает файл argv[1], удаляет заданный символ argv[2], пишет в файл с тем же именем, но другим типом.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
  FILE *R, *W;
  char c[1], ch;
  char name[15], type[3];
  int i;
  
  if(argc < 2){
    printf("Вы забыли ввести имя файла.\n");
    exit(1);
  }
  else 
    if(argc < 3){
      printf("Вы забыли ввести шаблонный символ.\n");
      exit(1);
    }
  
  if((R = fopen(argv[1], "r"))==NULL) {
    printf("Ошибка при открытии файла %s.\n", argv[1]);
    exit(1);
  }

                 //Изменение типа выходного файла
  strcpy(name, argv[1]);
  printf("Имя входного файла =  %s\n", name);
  for(i = 0; i<=strlen(name); i++)
    if(name[i]=='.'){
      ++i;
      name[i]='\0';
      printf("Введите тип файла вывода: ");
      scanf("%s", type);
      strcat(name, type);
      break;
    }
  printf("Имя выходного файла = %s\n", name);
   
  if((W = fopen(name, "w"))==NULL) {
    printf("Ошибка при открытии файла для записи %s.\n", name);
    exit(1);
  }

  c[0] = *argv[2];
  printf("Шаблонный символ для удаления = %s\n", c);
  
			//Начинаем считывать и записывать
  
  while ((ch = getc(R)) && (ch!=EOF))
    if(ch!=c[0])
      putc(ch, W);  

  printf("Запись успешно закончена!\n");
  return 0; 
}

   

