//Читает файл argv[1], удаляет строки с длиной большей argv[2], пишет в файл с тем же именем, но другим типом.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
  FILE *R, *W;
  char str[100];
  char name[15];
  char type[3];
  int n, i;

  if(argc < 2){
    printf("Вы забыли ввести имя файла.\n");
    exit(1);
  }
  else 
    if(argc < 3){
      printf("Вы забыли ввести длину строки.\n");
      exit(1);
    }
  
                     //Изменение типа выходного файла
  strcpy(name, argv[1]);
  printf("Имя входного файла =  %s\n", name);
  for(i = 0; i<=strlen(name); i++)
    if(name[i]=='.'){
      ++i;
      name[i]='\0';
      printf("Введите тип файла вывода: ");
      scanf("%s", type);
      strcat(name, type);
      break;
    }
  printf("Имя выходного файла = %s\n", name);
  
  if((R = fopen(argv[1], "r"))==NULL) {
    printf("Ошибка при открытии файла.\n");
    exit(1);
  }
  
  if((W = fopen(name, "w"))==NULL) {
    printf("Ошибка при открытии файла для записи.\n");
    exit(1);
  }

  n = atoi(argv[2]);
  printf("Максимальная длина строки = %d\n", n);
  
			//Начинаем считывать и записывать
  while(!feof(R)) {
    fgets(str, 100, R);
    if(strlen(str) <= n + 1)
    fputs(str, W);
  }
  
  printf("Запись успешно закончена!\n");
  return 0; 
}

   

