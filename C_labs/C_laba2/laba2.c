/*Ввод, Вывод, сортировка строк*/
#include <stdio.h>
#include <string.h>
#define SIZE 81  /* макс длина строки */
#define LIM 40 /* макс кол-во строк */
//#define HALT "" 
int SStr(char *strSS[], int LIMS);
void strsort(char *strings[], int num);
void PStr(char *strPS[], int numPS);

int main(void)
{
  char *ptr[LIM];
  int count=0;
  count = SStr(ptr, LIM); /* Ввод строк, возвращает кол-во строк */
  strsort(ptr, count); /* Сортировка строк */
  PStr(ptr, count); /* Печать строк */
  return 0;
}

/* Функция ввода строк */
int SStr(char *strSS[], int LIMS)
{
  char input[LIM][SIZE];
  int ct = 0;
  printf("Можете ввести не более %d строк\n", LIMS);
  printf("Для остановки ввода нажмите Enter в начале строки\n");
  while (ct < LIM && gets(input[ct]) != NULL 
                      && input[ct][0] != '\0') {
    strSS[ct] = input[ct];
    ct++;
  }
  return ct;
}

/* Функция печати строк */
void PStr(char *strPS[], int numPS)
{
  int k;
  printf("Вывод рузультата:\n");
  for (k = 0; k < numPS; k++)
    puts(strPS[k]);
}

/* Функция сортировки указателей строк */
void strsort(char *strings[], int num)
{
  char *temp; 
  int i, j;
  printf("Идет сортировка...\n");
  for (i = 0; i < num; i++) 
    for (j = i+1; j < num; j++)
      if (strcmp(strings[i], strings[j]) > 0) {
	temp = strings[i];
	strings[i] = strings[j];
	strings[j] = temp;
      }
}
