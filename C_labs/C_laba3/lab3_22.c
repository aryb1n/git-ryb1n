/* Ввод, вывод, сортировка массива структур. */
#include <stdio.h>
#include <stdlib.h>

struct worker {
  char name[30];
  int year;
  int num;
  long unsigned int salary;
};

struct worker *ls[10];

int enter(int r);
void list(int t);
int menu_select(void);
void sort(int m);

int main(void)
{
  char choice;
  int n = 0;

  for(;;) {
    choice = menu_select();
    switch(choice) {
      case 1:
	n = enter(n);
	break;
      case 2: list(n);
        break;
      case 3: sort(n);
        break;
      case 4: exit(0);
    }
  }

  return 0;
}


/* Получения значения, выбранного меню. */
int menu_select(void)
{
  int c;

  printf("/--------------------------\n1. Ввести имя\n");
  printf("2. Вывести список\n");
  printf("3. Выполнить сортировку\n");
  printf("4. Выход\n/--------------------------\n");
  do {
    printf("\nВведите номер нужного пункта: \n");
    scanf("%d", &c);
  } while(c < 0 || c > 4);
  return c;
}

/* Ввод списка. */
int enter(int i)
{
  ls[i] = (struct worker*)malloc(sizeof(struct worker));
    printf("Введите данные для структуры №%d:\n", i+1 );  
    printf("Введите фамилию: ");
    scanf("%s", ls[i]->name);

    printf("Введите год рождения: ");
    scanf("%d", &ls[i]->year);
  
    printf("Введите номер отдела: ");
    scanf("%d", &ls[i]->num);

    printf("Введите оклад: ");
    scanf("%lu", &ls[i]->salary);
  i++;
  return(i);
  
}

void sort(int m)/*Выполняет сортировку*/
{
  int i,j;
  struct worker *temp;
  temp = (struct worker*)malloc(sizeof(struct worker));
  for (i = 0; i < m; i++)
    for (j = i+1; j < m; j++)
      if (ls[i]->year > ls[j]->year) {
        temp = ls[i];
        ls[i] = ls[j]; 
	ls[j] = temp;
      }
  printf("Сортировка успешно закончена!!!\n");
}  

/* Вывод списка на экран. */
void list(int t)
{
  int i;  
  for (i=0; i < t; i++) {
    printf("Данные структуры №%d:\n", i+1 );
    printf("Фамилия: %s\n", ls[i]->name);
    printf("Год рождения: %d\n", ls[i]->year);
    printf("Номер отдела: %d\n", ls[i]->num);
    printf("Оклад: %lu\n\n", ls[i]->salary);
  }
  printf("\n\n");
}
