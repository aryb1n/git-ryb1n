#include <stdio.h>
#include <stdlib.h>

extern float f1(float a,float b);
extern float f2(float a,float b);

int main()
{
    printf("24.2 + 13.3 = %.1f\n",f1(24.2,13.3));
    printf("13.9 - 2.4 = %.1f\n",f2(13.9,2.4));

    return 0;
}
