#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h> 

int main()
{
  void *library_handler;
  float (*f1)(float a,float b);
  float (*f2)(float a,float b);
 
  library_handler = dlopen("/home/ryb1n/libs/lib5dyn.so",RTLD_LAZY); 
  if (!library_handler){ 
    fprintf(stderr,"dlopen() error: %s\n", dlerror());
    exit(1); 
  } 

  f1=dlsym(library_handler,"f1");
  f2=dlsym(library_handler,"f2");

  printf("24.2 + 13.3 = %.1f\n",(*f1)(24.2,13.3));
  printf("13.9 - 2.4 = %.1f\n",(*f2)(13.9,2.4));

  dlclose(library_handler);

  return 0;
}
